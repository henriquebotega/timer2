import React from 'react';
import { StyleSheet, Text, View, StatusBar, TouchableOpacity, Dimensions, Picker, Platform } from 'react-native';

const screen = Dimensions.get('window')

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#07121B',
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonStop: {
        borderColor: "#ff851b",
    },
    button: {
        borderWidth: 10,
        borderColor: "#89AAFF",
        width: screen.width / 2,
        height: screen.width / 2,
        borderRadius: screen.width / 2,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 30
    },
    buttonText: {
        fontSize: 45,
        color: "#89AAFF"
    },
    buttonTextStop: {
        color: "#ff851b"
    },
    timerText: {
        color: '#fff',
        fontSize: 90
    },
    picker: {
        width: 50,
        ...Platform.select({
            android: {
                color: '#fff',
                backgroundColor: '#07121b',
                marginLeft: 10
            }
        })
    },
    pickerItem: {
        color: "#fff",
        fontSize: 20
    },
    pickerContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    }
});

const createArray = (length) => {
    const arr = []
    let i = 0;

    while (i < length) {
        arr.push(i.toString())
        i++
    }

    return arr;
}

const AVAILABLE_MINUTES = createArray(10);
const AVAILABLE_SECONDS = createArray(60);

export default class App extends React.Component {

    state = {
        remainingSeconds: 90,
        isRunning: false,
        selectedMinutes: "0",
        selectedSeconds: "0",
    }

    interval = null;

    formatNumber = (number) => `0${number}`.slice(-2)

    getRemaining = (time) => {
        const minutes = Math.floor(time / 60)
        const seconds = time - minutes * 60

        return { minutes: this.formatNumber(minutes), seconds: this.formatNumber(seconds) }
    }

    start = () => {
        this.setState(state => ({
            remainingSeconds: parseInt(state.selectedMinutes, 10) * 60 + parseInt(state.selectedSeconds, 10),
            isRunning: true
        }))

        this.interval = setInterval(() => {
            this.setState(state => ({
                remainingSeconds: state.remainingSeconds - 1
            }))
        }, 1000)
    }

    stop = () => {
        clearInterval(this.interval)
        this.interval = null;
        this.setState({ remainingSeconds: 90, isRunning: false })
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.remainingSeconds === 0 && prevState.remainingSeconds !== 0) {
            this.stop();
        }
    }

    componentWillUnmount() {
        if (this.interval) {
            clearInterval(this.interval)
        }
    }

    handleOnValueChangeMinutes = (item) => {
        this.setState({
            selectedMinutes: item
        })
    }

    handleOnValueChangeSeconds = (item) => {
        this.setState({
            selectedSeconds: item
        })
    }

    handlePicker = () => {
        return (
            <View style={styles.pickerContainer}>
                <Picker selectedValue={this.state.selectedMinutes} onValueChange={this.handleOnValueChangeMinutes} style={styles.picker} itemStyle={styles.pickerItem} mode="dropdown">
                    {
                        AVAILABLE_MINUTES.map(v => (
                            <Picker.Item key={v} label={v} value={v} />
                        ))
                    }
                </Picker>
                <Text style={styles.pickerItem}>minutes</Text>

                <Picker selectedValue={this.state.selectedSeconds} onValueChange={this.handleOnValueChangeSeconds} style={styles.picker} itemStyle={styles.pickerItem} mode="dropdown">
                    {
                        AVAILABLE_SECONDS.map(s => (
                            <Picker.Item key={s} label={s} value={s} />
                        ))
                    }
                </Picker>
                <Text style={styles.pickerItem}>seconds</Text>
            </View>
        )
    }

    render() {
        const { minutes, seconds } = this.getRemaining(this.state.remainingSeconds)

        return (
            <View style={styles.container}>
                <StatusBar barStyle="light-content" />

                {this.state.isRunning ? (
                    <Text style={styles.timerText}>{`${minutes}:${seconds}`}</Text>
                ) : (
                        this.handlePicker()
                    )}

                {
                    this.state.isRunning ? (
                        <TouchableOpacity style={[styles.button, styles.buttonStop]} onPress={this.stop}>
                            <Text style={[styles.buttonText, styles.buttonTextStop]}>Stop</Text>
                        </TouchableOpacity>
                    ) : (
                            <TouchableOpacity style={styles.button} onPress={this.start}>
                                <Text style={styles.buttonText}>Start</Text>
                            </TouchableOpacity>
                        )
                }

            </View>
        );
    }
}
